import os
import re
from urllib.parse import urlparse

ARTICLE_FILE_NAME = "article.txt"
ARTICLES_DIRECTORY = "articles"


class FileOutput:
    def __init__(self, source):
        self._source = source
        self._output_file_path = self._generate_output_path()

    def _generate_output_path(self):
        parsed_url = urlparse(self._source)

        file_path_parts = re.split(r"/", parsed_url.path)
        file_path_parts[0] = parsed_url.netloc

        return file_path_parts

    def send(self, data):
        current_dir = os.curdir + "/" + ARTICLES_DIRECTORY

        for folder in self._output_file_path:
            current_dir = current_dir + "/" + folder
            if not os.path.exists(current_dir):
                os.mkdir(current_dir)

        with open(current_dir + "/" + ARTICLE_FILE_NAME, 'w', encoding='utf-8') as output_file:
            output_file.write(data)
