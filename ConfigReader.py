import configparser
import re
import os


class ConfigReader:
    FORMAT_SETTINGS_SECTION = "format"

    CONFIG_FORMAT = {
        "format": {
            "line_length": "positive",
            "paragraph_delimiter_size": "positive",
            "header_delimiter_size": "positive",
        },
        "wrappers": "wrapper"
    }
    FORMAT_VALIDATOR = {
        "positive": lambda v: re.match(r"^\d+$", v) and int(v) > 0,
        "wrapper": lambda v: re.match(r"^.+__\w+__.+$", v),
    }
    DEFAULT_CONFIG = {
        "format": {
            "line_length": 80,
            "paragraph_delimiter_size": 2,
            "header_delimiter_size": 2,
        },
        "wrappers": {}
    }

    def __init__(self, config_path):
        """Constructor"""

        self._config = self.DEFAULT_CONFIG

        if not os.path.exists(config_path):
            print("Config file \"%s\" failed to open, used default settings instead" % config_path)

            return

        config_data = configparser.ConfigParser()
        config_data.read(config_path)

        for section in self.CONFIG_FORMAT:
            formats = self.CONFIG_FORMAT[section]
            if type(formats) == dict:
                for option in formats:
                    validator = self.FORMAT_VALIDATOR[formats[option]]
                    if config_data.has_option(section, option) and validator(config_data.get(section, option)):
                        self._config[section][option] = config_data.get(section, option, raw=True)
            else:
                if config_data.has_section(section):
                    for option in config_data.options(section):
                        self._config[section][option] = config_data.get(section, option, raw=True)

    def get_config(self):
        return self._config
