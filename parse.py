import sys
from ConfigReader import ConfigReader
from FileOutput import FileOutput
from PageParser import PageParser


DEFAULT_CONFIG_PATH = './config.cfg'

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("URL must be specified.")

    article_url = sys.argv[1]

    if len(sys.argv) > 2:
        config_path = sys.argv[2]
    else:
        config_path = DEFAULT_CONFIG_PATH

    format_config = ConfigReader(config_path).get_config()

    page_parser = PageParser(article_url, format_config)

    output = FileOutput(article_url)

    output.send(page_parser.parse())
