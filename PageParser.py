import configparser
import re

import requests
from lxml import html, etree
from urllib.parse import urlparse
from itertools import chain


def _stringify_children(node):
    parts = ([node.text] +
             list(chain(*([c.text, etree.tostring(c), c.tail] for c in node.getchildren()))))
    return ''.join(filter(lambda x: x is not None and type(x) != bytes, parts))


class PageParser:
    SITES_CONFIG_FILE = "sites.cfg"
    HEADER_DESCRIPTORS_OPTION = "header_descriptors"
    PARAGRAPH_DESCRIPTORS_OPTION = "paragraph_descriptors"

    def __init__(self, url, format_config):
        """Constructor"""

        try:
            self._content = requests.get(url).text
        except IOError:
            print("Invalid URL")
            exit()

        self._host = urlparse(url).netloc
        self._format_config = format_config
        self._read_config()

    def parse(self):
        return self._format_page()

    def _format_page(self):
        parser = etree.HTMLParser(remove_blank_text=True, recover=True)
        tree = html.document_fromstring(self._content, parser)

        main_header_node = tree.xpath(self._header_descriptors)
        if len(main_header_node) > 0:
            main_header = tree.xpath(self._header_descriptors)[0].text + \
              "\n" * int(self._format_config["format"]["header_delimiter_size"])
        else:
            main_header = ""

        paragraph_nodes = tree.xpath(self._paragraph_descriptors)

        paragraph_nodes = filter(lambda x: x.text is not None or len(x.findall("*")) > 0, paragraph_nodes)

        output = ""
        line_length = int(self._format_config["format"]["line_length"])

        node_text_array = self._get_text(paragraph_nodes, True)

        formatted_text = \
            main_header + "".join([x for x in node_text_array if x is not None])

        string_counter = 0

        formatted_text = re.sub("\n", " ||| ", formatted_text)
        words = re.split(r"[ \t]", formatted_text)

        for word in words:
            if word == "|||":
                output = output + "\n"
                string_counter = 0
            elif string_counter == 0:
                string_counter = string_counter + len(word)
                output = output + word
            elif string_counter + len(word) + 1 <= line_length:
                string_counter = string_counter + len(word) + 1
                output = output + " " + word
            else:
                output = output + "\n" + word
                string_counter = len(word)

        return output

    def _get_text(self, nodes, is_paragraph=False):
        node_text_array = []

        for node in nodes:
            node_text_array.append(self._wrap(node))
            node_text_array += self._get_text(node.findall("*"))
            if type(node.tail) == str and re.match("^.*\S*.*$", node.tail) is not None:
                node_text_array.append(node.tail)

            if is_paragraph:
                node_text_array.append("\n" * int(self._format_config["format"]["paragraph_delimiter_size"]))

        return node_text_array

    def _wrap(self, node):
        if node.tag not in self._format_config["wrappers"]:
            return node.text

        wrapper = self._format_config["wrappers"][node.tag]

        parentheses = [
            re.sub(r"__.*__.+$", "", wrapper),
            re.sub(r"^.+__.*__", "", wrapper),
        ]
        wrapped_prop = re.sub(r"(^[^_]+__)|(__[^_]+$)", "", wrapper)

        if wrapped_prop == "text":
            return parentheses[0] + node.text + parentheses[1]

        return _stringify_children(node) + parentheses[0] + node.get(wrapped_prop) + parentheses[1]

    def _read_config(self):
        open(self.SITES_CONFIG_FILE, 'a').close()

        config_data = configparser.ConfigParser()
        config_data.read(self.SITES_CONFIG_FILE)

        if config_data.has_section(self._host) \
                and config_data.has_option(self._host, self.PARAGRAPH_DESCRIPTORS_OPTION) \
                and config_data.has_option(self._host, self.HEADER_DESCRIPTORS_OPTION):
            self._paragraph_descriptors = config_data.get(self._host, self.PARAGRAPH_DESCRIPTORS_OPTION)
            self._header_descriptors = config_data.get(self._host, self.HEADER_DESCRIPTORS_OPTION)
        else:
            print("Looks like you're trying to get an article from this site for the first time. We are trying to "
                  "recognize the contents of the page...")
            self._analyze_page()

    def _analyze_page(self):
        # TODO добавить парсер страницы на основе концентрации <p>,
        #  и определение контейнера статьи как их общего предка.
        #  Запись результата в sites.conf
        exit("Sorry, the script could not analyze this page, please fill in sites.conf manually.")
